import { Component, OnInit } from '@angular/core';
import {NavigationStart, Router} from "@angular/router";
import {AsidePageModel} from "./aside.pagemodel";
import {AsideMenuElement} from "../types/aside-menu-element";

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.css']
})
export class AsideComponent implements OnInit {

  pageModel: AsidePageModel = new AsidePageModel();

  constructor(private router: Router) {
    // build aside menu
    const pendingRequests: AsideMenuElement = new AsideMenuElement();
    pendingRequests.active = false;
    pendingRequests.caption = 'Pending requests';
    pendingRequests.url = '/pending-authorizations';
    pendingRequests.iconFirstClass = 'fas';
    pendingRequests.iconSecondClass = 'fa-user-lock';

    const requestsHistory: AsideMenuElement = new AsideMenuElement();
    requestsHistory.active = false;
    requestsHistory.caption = 'Requests history';
    requestsHistory.url = '/requests-history';
    requestsHistory.iconFirstClass = 'fas';
    requestsHistory.iconSecondClass = 'fa-history';

    const keySlots: AsideMenuElement = new AsideMenuElement();
    keySlots.active = false;
    keySlots.caption = 'Key slots';
    keySlots.url = '/keys';
    keySlots.iconFirstClass = 'fas';
    keySlots.iconSecondClass = 'fa-key';

    const pushNotifications: AsideMenuElement = new AsideMenuElement();
    pushNotifications.active = false;
    pushNotifications.caption = 'Push notifications';
    pushNotifications.url = '/push-notifications';
    pushNotifications.iconFirstClass = 'far';
    pushNotifications.iconSecondClass = 'fa-envelope';

    this.pageModel.elements.push(pendingRequests, requestsHistory, keySlots, pushNotifications);
  }

  ngOnInit(): void {
    const pm = this.pageModel;

    this.router.events.subscribe({
      next(val) {
        if(val) {
          const url = (<NavigationStart>val).url;
          const menuElement = pm.getAsideMenuElement(url);
          if(menuElement != null) {
            // set all other elements as inactive
            for(const elem of pm.elements) {
              elem.active = false;
            }
            menuElement.active = true;
          }
        }
      }
    })
  }

}
