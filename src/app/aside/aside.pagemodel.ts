import {AsideMenuElement} from "../types/aside-menu-element";

export class AsidePageModel {
  public elements: AsideMenuElement[] = [];

  getAsideMenuElement(url: string): AsideMenuElement | null {
    for(let elem of this.elements) {
      if(elem.url === url) {
        return elem;
      }
    }

    return null;
  }
}



