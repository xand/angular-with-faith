import { Component, OnInit } from '@angular/core';
import {KeysPageModel} from "./keys-page.pagemodel";
import {KeySlotService} from "../services/key-slot.service";
import {KeySlot} from "../types/key-slot";

@Component({
  selector: 'app-keys-page',
  templateUrl: './keys-page.component.html',
  styleUrls: ['./keys-page.component.css']
})
export class KeysPageComponent implements OnInit {

  public pageModel: KeysPageModel = new KeysPageModel();

  constructor(private keySlotService: KeySlotService) { }

  ngOnInit(): void {
    const pageModel = this.pageModel;

    this.keySlotService.getKeySlots().subscribe({
      next(v) {
        v.forEach(item => {
          const ks = <KeySlot>item;
          pageModel.keySlots.push(ks);
        });
      }
    });
  }

  update(id: string) {
    console.log('Update => ' + id);
  }

  delete(id: string) {
    console.log('Delete => ' + id);
  }

}




