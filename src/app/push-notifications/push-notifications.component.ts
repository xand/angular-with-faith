import {Component, OnInit} from '@angular/core';
import {PushSubscribeService} from "../services/push-subscribe.service";
import {Observable} from "rxjs";
import {PushNotificationObs} from "../types/push-notification-obs";
import {PushNotificationPageModel} from "../types/page-model";
import {AppConfigService} from "../services/app-config.service";
import {AppConfig} from "../types/app-config";

@Component({
  selector: 'app-push-notifications',
  templateUrl: './push-notifications.component.html',
  styleUrls: ['./push-notifications.component.css']
})
export class PushNotificationsComponent implements OnInit {

  pageModel: PushNotificationPageModel = new PushNotificationPageModel();
  appConfig: AppConfig;

  constructor(private pushSubscribeService: PushSubscribeService,
              private appConfigService: AppConfigService) {
    this.appConfig = appConfigService.getAppConfig();
  }

  ngOnInit(): void {
  }

  subscribeClick(): void {
    const lPageModel = this.pageModel;

    let z: Observable<PushNotificationObs> = this.pushSubscribeService.performSubscribe();
    z.subscribe({
      next(k) {
        lPageModel.endpointUrl = k.endpoint;
        lPageModel.p256dhKey = k.p256dh;
        lPageModel.authKey = k.auth;
      }
    });
  }

}
