export class AsideMenuElement {
  public caption: string = '';
  public active: boolean = false;
  public url: string = '';
  public iconFirstClass: string = '';
  public iconSecondClass: string = '';

  constructor() {}

}
