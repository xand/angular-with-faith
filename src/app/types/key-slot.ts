export class KeySlot {
  public id: string = '';
  public label: string = '';
  public content: string = '';

  constructor(json: any) {
    this.id = json['id'];
    this.label = json['label'];
    this.content = json['content'];
  }
}
