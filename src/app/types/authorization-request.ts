export class AuthorizationRequest {
  public id: string = '';
  public received: string = '';
  public from: string = '';
  public label: string = '';
  public keySlot: string = '';
  public validUntil!: Date;

  public status: AuthorizationRequestStatus | null = null;

  constructor(json: any) {
    this.id = json['id'];
    this.received = json['received'];
    this.from = json['from'];
    this.label = json['label'];
    this.keySlot = json['keySlot'];

    const jsonStatus: string = json['status'];

    if(jsonStatus === 'PENDING') {
      this.status = AuthorizationRequestStatus.PENDING;
    } else if(jsonStatus === 'GRANTED') {
      this.status = AuthorizationRequestStatus.GRANTED;
    } else if(jsonStatus === 'DENIED') {
      this.status = AuthorizationRequestStatus.DENIED;
    } else if(jsonStatus === 'EXPIRED') {
      this.status = AuthorizationRequestStatus.EXPIRED;
    }

    const sDate: string = json['validUntil'];
    this.validUntil = new Date(sDate);
  }
}

export enum AuthorizationRequestStatus {
  PENDING = 'PENDING',
  GRANTED = 'GRANTED',
  DENIED = 'DENIED',
  EXPIRED = 'EXPIRED'
}
