export class AppConfig {
  serviceWorkerAvailable: boolean = false;
  serviceWorkerReg: any = null;
  serviceWorkerRegError: boolean = false;

  subscribedToPushNotifications: boolean = false;
  pushNotificationsEndpoint: string = '';
  pushNotificationsP256DHKey: string = '';
  pushNotificationsAuthKey: string = '';
  pushNotificationsBlocked: boolean = false;

  constructor() {}
}
