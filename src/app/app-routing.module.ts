import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { PendingAuthorizationsComponent } from './pending-authorizations/pending-authorizations.component';
import { PushNotificationsComponent } from './push-notifications/push-notifications.component';
import {RequestsHistoryComponent} from "./requests-history/requests-history.component";
import {KeysPageComponent} from "./keys-page/keys-page.component";

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'pending-authorizations', component: PendingAuthorizationsComponent, pathMatch: 'full' },
  { path: 'push-notifications', component: PushNotificationsComponent, pathMatch: 'full' },
  { path: 'requests-history', component: RequestsHistoryComponent, pathMatch: 'full' },
  { path: 'keys', component: KeysPageComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
