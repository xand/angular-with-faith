import {Component, OnInit} from '@angular/core';
import {AppConfigService} from "./services/app-config.service";
import {from} from "rxjs";
import {AppConfig} from "./types/app-config";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular-with-faith';

  constructor(private appConfigService: AppConfigService) {

  }

  ngOnInit(): void {
    console.log('Init app...');

    const appConfig: AppConfig = this.appConfigService.getAppConfig();

    let serviceWorkerObsComplete: boolean = false;

    // check capability for serviceWorker
    if (!('serviceWorker' in navigator)) {
      console.log('serviceWorker is not in navigator');
      appConfig.serviceWorkerAvailable = false;
    } else {
      console.log('serviceWorker is in navigator');
      appConfig.serviceWorkerAvailable = true;
    }

    // register serviceWorker
    if(appConfig.serviceWorkerAvailable) {
      navigator.serviceWorker.register('sw.js').then(function(reg){
        console.log('serviceWorker registered successfully');
        appConfig.serviceWorkerReg = reg;
        appConfig.serviceWorkerRegError = false;
      }).catch(function(err){
        console.log('error while registering serviceWorker: ' + err);
        appConfig.serviceWorkerRegError = true;
      });
    }
  }
}
