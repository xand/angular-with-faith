import {Injectable} from "@angular/core";

import {HandleError, HttpErrorHandler,} from '../services/http-error-handler.service';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AuthorizationRequest, AuthorizationRequestStatus} from "../types/authorization-request";
import {Observable, of} from "rxjs";
import {catchError, mergeMap, tap} from "rxjs/operators";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token',
  }),
};

@Injectable({
  providedIn: "root"
})
export class AuthorizationRequestsService {
  baseUrl = '/api/v1';
  private handleError: HandleError;

  private pendingRequestsCache: Map<String, AuthorizationRequest> = new Map();
  private pendingRequestsCacheStarted: boolean = false;

  private historyRequestsCache: Map<String, AuthorizationRequest> = new Map();

  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError(
      'AuthorizationService'
    );
  }

  getAuthorizationRequests(): Observable<AuthorizationRequest[]> {
    const url = this.baseUrl + '/authorization?status=pending';

    if(this.pendingRequestsCacheStarted) {
      const output: AuthorizationRequest[] = [];
      for(let entry of this.pendingRequestsCache.entries()) {
        output.push(entry[1]);
      }

      return of(output);
    } else {
        return this.http
          .get<AuthorizationRequest[]>(url)
          .pipe(
            tap(v => {
              const requests: any[] = v;
              for(let r of requests) {
                let ar: AuthorizationRequest = new AuthorizationRequest(r);
                this.pendingRequestsCache.set(ar.id, ar);
              }

              this.pendingRequestsCacheStarted = true;
            }),
            catchError(
              this.handleError('getAuthorizations', [])
            )
        );
    }
  }

  expire(id: string): void {
    const request: AuthorizationRequest | undefined = this.pendingRequestsCache.get(id);
    if(request) {
      request.status = AuthorizationRequestStatus.EXPIRED;
      this.pendingRequestsCache.delete(id);
      this.historyRequestsCache.set(request.id, request);
    }
  }

  deny(id: string): Observable<AuthorizationRequest> {
    const body = { id: id };

    const url = this.baseUrl + '/authorization/' + id;

    return this.http.post(url, body).pipe(
      mergeMap((data) => {
        const output = <AuthorizationRequest>data;

        // move to history
        const z = this.pendingRequestsCache.get(output.id);
        if(z != null) {
          z.status = AuthorizationRequestStatus.DENIED;
          this.pendingRequestsCache.delete(output.id);
          this.historyRequestsCache.set(output.id, output);
        }

        return of(output);
      })
    );
  }

  authorize(id: string): Observable<AuthorizationRequest> {
    const body = { granted: true };

    const url = this.baseUrl + '/authorization/' + id;

    return this.http.patch(url, body).pipe(
      mergeMap((data) => {
        const output = <AuthorizationRequest>data;

        // move to history
        const z = this.pendingRequestsCache.get(output.id);
        // set status
        if(z != null) {
          z.status = AuthorizationRequestStatus.GRANTED;
          this.pendingRequestsCache.delete(output.id);
          this.historyRequestsCache.set(output.id, output);
        }

        return of(output);
      })
    );
  }

}



