import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {HandleError, HttpErrorHandler} from "./http-error-handler.service";
import {KeySlot} from "../types/key-slot";
import {Observable, of} from "rxjs";
import {catchError, tap} from "rxjs/operators";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token',
  }),
};

@Injectable({
  providedIn: "root"
})
export class KeySlotService {
  baseUrl = '/api/v1';
  private handleError: HandleError;

  private keySlotsCache: Map<String, KeySlot> = new Map();

  private keySlotsCacheStarted: boolean = false;

  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError(
      'AuthorizationService'
    );
  }

  getKeySlots(): Observable<KeySlot[]> {
    const url = this.baseUrl + '/key';

    if(this.keySlotsCacheStarted) {
      const output: KeySlot[] = [];
      for(let entry of this.keySlotsCache.entries()) {
        output.push(entry[1]);
      }

      return of(output);
    } else {
      return this.http
        .get<KeySlot[]>(url)
        .pipe(
          tap(v => {
            const keySlots: any[] = v;
            for(let r of keySlots) {
              let ks: KeySlot = new KeySlot(r);
              this.keySlotsCache.set(ks.id, ks);
            }

            this.keySlotsCacheStarted = true;
          }),
          catchError(
            this.handleError('getAuthorizations', [])
          )
        );
    }
  }
}



