import {Inject, Injectable} from '@angular/core';
import {Observable, from, EMPTY, of, concat} from "rxjs";

import {HttpClient} from "@angular/common/http";
import {
  catchError, concatMap,
  last,
  map,
  mapTo,
  mergeAll,
  mergeMap,
  take, tap
} from "rxjs/operators";
import {PushNotificationObs} from "../types/push-notification-obs";
import {AppConfigService} from "./app-config.service";
import {AppConfig} from "../types/app-config";

@Injectable()
export class PushSubscribeService {

  constructor(private http: HttpClient,
              private appConfigService: AppConfigService) {
  }

  private basePath = '/api/v1';

  performSubscribe(): Observable<PushNotificationObs> {
    let appConfig: AppConfig = this.appConfigService.getAppConfig();
    let result: PushNotificationObs = new PushNotificationObs();
    let http = this.http;

    let output: Observable<PushNotificationObs> = from(
      appConfig.serviceWorkerReg.pushManager.subscribe(
          { userVisibleOnly: true,
            applicationServerKey: 'BNSzKLes51hcmoUL7eQTuAcg1GdRI7sOrdDOc_yKBg6KxSQk3PuhptY0xVG59PmlbunnT7xdDNkQTAg_Ne-Tsjs'
      })).pipe(
       map((v: any) => {
         const o = JSON.parse(JSON.stringify(v));

         console.log('Subscribed to push service => ' + JSON.stringify(v));

         appConfig.subscribedToPushNotifications = true;
         appConfig.pushNotificationsEndpoint = v.endpoint;
         appConfig.pushNotificationsP256DHKey = o['keys']['p256dh'];
         appConfig.pushNotificationsAuthKey = o['keys']['auth'];

         // send data to server
         const postData = {
           endpoint: appConfig.pushNotificationsEndpoint,
           p256dh: appConfig.pushNotificationsP256DHKey,
           auth: appConfig.pushNotificationsAuthKey
         };
         http.post('/api/v1/push-notification', postData).subscribe({
           next: data => {

           },
           error: err => {

           }
         });

         return result;
       }),
       catchError((err) => {
         console.log('Error while getting subscription to pushManager (probably user blocked notifications) => ' + err);
         appConfig.pushNotificationsBlocked = true;
         return of(result);
       })
    );

    return output;
  }

}
