import {Injectable} from "@angular/core";
import {AppConfig} from "../types/app-config";

@Injectable()
export class AppConfigService {

  private appConfig: AppConfig;

  constructor() {
    this.appConfig = new AppConfig();
  }

  getAppConfig(): AppConfig {
    return this.appConfig;
  }

}
