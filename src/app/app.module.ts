import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AsideComponent } from './aside/aside.component';
import { ControlSidebarComponent } from './control-sidebar/control-sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { PendingAuthorizationsComponent } from './pending-authorizations/pending-authorizations.component';
import { HttpErrorHandler } from './services/http-error-handler.service';
import { MessageService } from './services/message.service';
import { fakeBackendProvider } from './providers/fake-api.provider';
import { PushNotificationsComponent } from './push-notifications/push-notifications.component';
import {PushSubscribeService} from "./services/push-subscribe.service";
import {AppConfigService} from "./services/app-config.service";
import {FormsModule} from "@angular/forms";
import { RequestsHistoryComponent } from './requests-history/requests-history.component';
import {AuthorizationRequestsService} from "./services/authorization-requests.service";
import { CountdownTimerComponent } from './countdown-timer/countdown-timer.component';
import { KeysPageComponent } from './keys-page/keys-page.component';
import {KeySlotService} from "./services/key-slot.service";

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AsideComponent,
    ControlSidebarComponent,
    FooterComponent,
    HomeComponent,
    PendingAuthorizationsComponent,
    PushNotificationsComponent,
    RequestsHistoryComponent,
    CountdownTimerComponent,
    KeysPageComponent,
  ],
  imports: [BrowserModule, HttpClientModule, AppRoutingModule, FormsModule],
  providers: [
    HttpErrorHandler,
    MessageService,
    fakeBackendProvider,
    PushSubscribeService,
    AuthorizationRequestsService,
    AppConfigService,
    KeySlotService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
