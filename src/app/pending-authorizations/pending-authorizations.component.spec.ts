import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingAuthorizationsComponent } from './pending-authorizations.component';

describe('PendingAuthorizationsComponent', () => {
  let component: PendingAuthorizationsComponent;
  let fixture: ComponentFixture<PendingAuthorizationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PendingAuthorizationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingAuthorizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
