import {Component, OnInit} from '@angular/core';
import {AuthorizationRequest, AuthorizationRequestStatus} from "../types/authorization-request";
import {PendingAuthorizationsPageModel} from "./pending-authorizations.pagemodel";
import {AuthorizationRequestsService} from "../services/authorization-requests.service";

@Component({
  selector: 'app-pending-authorizations',
  templateUrl: './pending-authorizations.component.html',
  styleUrls: ['./pending-authorizations.component.css'],
})
export class PendingAuthorizationsComponent implements OnInit {

  public pageModel: PendingAuthorizationsPageModel = new PendingAuthorizationsPageModel();

  constructor(private authService: AuthorizationRequestsService) {}

  ngOnInit(): void {
    const pageModel = this.pageModel;

    this.authService
      .getAuthorizationRequests()
      .subscribe({
        next(v) {
          v.forEach(item => {
            const ar = <AuthorizationRequest>item;
            pageModel.pendingAuthorizations.push(ar);
          });
        }
      });
  }

  authorize(id: string): void {
    console.log('Authorize clicked => ' + id);

    const pageModel: PendingAuthorizationsPageModel = this.pageModel;

    this.authService.authorize(id).subscribe({
      next(v) {
        const req: AuthorizationRequest | null = pageModel.getPendingAuthorizationById(v.id);
        if(req != null) {
          req.status = v.status;
        }
      }
    });

  }

  deny(id: string): void {
    console.log('Deny clicked => ' + id);
  }

  dismiss(id: string): void {
    this.pageModel.removeAuthorizationRequest(id);
  }

  onExpired(request: AuthorizationRequest) {
    console.log('Expired: ' + request.id);
    this.authService.expire(request.id);

    const a: AuthorizationRequest | null = this.pageModel.getPendingAuthorizationById(request.id);
    a!.status = AuthorizationRequestStatus.EXPIRED;
  }
}
