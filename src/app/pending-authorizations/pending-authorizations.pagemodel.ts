import {AuthorizationRequest} from "../types/authorization-request";

export class PendingAuthorizationsPageModel {
  public pendingAuthorizations: AuthorizationRequest[] = [];

  constructor(){}

  getPendingAuthorizationById(id: string): AuthorizationRequest | null {
    for(let i = 0; i < this.pendingAuthorizations.length; i++) {
      if(this.pendingAuthorizations[i].id === id) {
        return this.pendingAuthorizations[i];
      }
    }

    return null;
  }

  removeAuthorizationRequest(id: string) {
    let idxToRemove = -1;
    for(let i = 0; i < this.pendingAuthorizations.length; i++) {
      const v = this.pendingAuthorizations[i];
      if(v.id === id) {
        idxToRemove = i;
      }
    }

    if(idxToRemove > -1) {
      this.pendingAuthorizations.splice(idxToRemove, 1);
    }
  }
}




