import {Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {Observable, timer} from "rxjs";
import {CountdownTimerPageModel} from "./countdown-timer.pagemodel";
import {AuthorizationRequestsService} from "../services/authorization-requests.service";
import {AuthorizationRequest} from "../types/authorization-request";

@Component({
  selector: 'app-countdown-timer',
  templateUrl: './countdown-timer.component.html',
  styleUrls: ['./countdown-timer.component.css']
})
export class CountdownTimerComponent implements OnInit {

  @Input()
  request!: AuthorizationRequest;

  @Output()
  expired = new EventEmitter<AuthorizationRequest>();

  pageModel: CountdownTimerPageModel = new CountdownTimerPageModel();

  constructor() { }

  ngOnInit(): void {
    const pm = this.pageModel;
    const request = this.request;

    const sValidUntil = this.request.validUntil;
    const validUntil = new Date(sValidUntil);

    const expiredEventEmitter = this.expired;

    timer(0, 1000).subscribe({
      next(val) {
        let sTimeLeft = '';

        const validUntilMs = validUntil.getTime();
        const currentTimeMs = new Date().getTime();

        if(currentTimeMs - validUntilMs > 0) {
          pm.secondsLeft = 'EXPIRED!';
          expiredEventEmitter.emit(request);
          return;
        }

        let delta = Math.abs(validUntilMs - currentTimeMs) / 1000;

        // calculate (and subtract) whole days
        const days = Math.floor(delta / 86400);
        delta -= days * 86400;

        // calculate (and subtract) whole hours
        const hours = Math.floor(delta / 3600) % 24;
        delta -= hours * 3600;

        let sHours = hours.toString();
        if(sHours.length < 2) {
          sHours = '0' + sHours;
        }

        // calculate (and subtract) whole minutes
        const minutes = Math.floor(delta / 60) % 60;
        delta -= minutes * 60;

        let sMinutes = minutes.toString();
        if(sMinutes.length < 2) {
          sMinutes = '0' + sMinutes;
        }

        // what's left is seconds
        const seconds = Math.floor(delta % 60);  // in theory the modulus is not required

        let sSeconds = seconds.toString();
        if(sSeconds.length < 2) {
          sSeconds = '0' + sSeconds;
        }

        if(days > 0) {
          sTimeLeft = days + 'd';
        }

        if(days > 0) {
          sTimeLeft += ' ' + sHours + 'h';
        } else {
          if(sHours != '00') {
            sTimeLeft += ' ' + sHours + 'h';
          }
        }

        if(hours > 0) {
          sTimeLeft += ' ' + sMinutes + 'm';
        } else {
          if(sMinutes != '00') {
            sTimeLeft += ' ' + sMinutes + 'm';
          }
        }

        if(minutes > 0) {
          sTimeLeft += ' ' + sSeconds + 's';
        } else {
          if(sSeconds != '00') {
            sTimeLeft += ' ' + sSeconds + 's';
          }
        }

        pm.secondsLeft = sTimeLeft;
      }
    });
  }
}




