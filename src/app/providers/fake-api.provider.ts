import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import {AuthorizationRequest} from "../types/authorization-request";
import {DatePipe} from "@angular/common";

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const { url, method, headers, body } = request;

    return of(null)
      .pipe(mergeMap(handleRoute))
      .pipe(materialize())
      .pipe(delay(500))
      .pipe(dematerialize());

    function handleRoute() {
      switch(true) {
        case url.endsWith('/api/v1/authorization?status=pending') && method === 'GET':
          return getPendingAuthorizations();
        case url.endsWith('/api/v1/key') && method === 'GET':
          return getKeySlots();
        case url.match(/\/api\/v1\/authorization\/.+$/) && method === 'PATCH':
          const idx = url.lastIndexOf('/') + 1;
          const id = url.substring(idx);
          return authorize(id);
        case url.endsWith('/api/v1/push-notification') && method === 'POST':
          return postPushNotification();
        default:
          return next.handle(request);
      }
    }

    function authorize(id: string) {
      const { action } = body;

      return ok({
        id: id,
        received: '23/03/2021 14:00:22',
        from: '163.2.112.200',
        label: 'Test label',
        keySlot: 'a2567527-9a1c-454b-a270-1c473e073423',
        timeLeft: '03:43',
        status: 'GRANTED'
      });
    }

    function getKeySlots() {
      return ok([
        {
          id: 'a2567527-9a1c-454b-a270-1c473e073423',
          label: 'Label 01',
          content: 'Content 01'
        },
        {
          id: 'a3567527-9a1c-454b-a270-1c473e073423',
          label: 'Label 02',
          content: 'Content 02'
        },
        {
          id: 'a4567527-9a1c-454b-a270-1c473e073423',
          label: 'Label 03',
          content: 'Content 03'
        }
      ]);
    }

    function postPushNotification() {
      const { endpoint, p256dh, auth } = body;
      return ok({});
    }

    function getRandomDate(min: number, max: number) {
      const datepipe: DatePipe = new DatePipe('en_US');
      const rndSeconds: number = Math.floor(Math.random() * (max - min + 1)) + min;

      const d = new Date();
      d.setSeconds(d.getSeconds() + rndSeconds);
      const fDate = datepipe.transform(d, 'YYYY-MM-ddTHH:mm:ss');

      return fDate;
    }

    function getPendingAuthorizations() {
      return ok([
        {
          id: 'bd1a104f-1a10-41ef-975d-89bbc2f5de43',
          received: '23/03/2021 14:00:22',
          from: '163.2.112.200',
          label: 'Test label',
          keySlot: 'a2567527-9a1c-454b-a270-1c473e073423',
          validUntil: getRandomDate(5, 10),
          status: 'PENDING'
        },
        {
          id: '5dae7596-0613-43d8-9c43-01dac880c2fa',
          received: '23/03/2021 14:00:20',
          from: '163.2.112.230',
          label: 'Test label',
          keySlot: 'a3567527-9a1c-454b-a270-1c473e073423',
          validUntil: getRandomDate(10, 20),
          status: 'PENDING'
        },
        {
          id: '5dbe7596-0613-43d8-9c43-01dac880c2fa',
          received: '23/03/2021 14:00:20',
          from: '163.2.112.230',
          label: 'Test label',
          keySlot: 'a4567527-9a1c-454b-a270-1c473e073423',
          validUntil: getRandomDate(100, 600),
          status: 'PENDING'
        },
      ]);
    }

    function ok(body?: any) {
      return of(new HttpResponse({ status: 200, body }));
    }
  }
}

export const fakeBackendProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true,
};




